use crate::Process;
use crate::ProcessFlags;
use crate::Type;
use glib::translate::*;
use glib::IsA;
use std::boxed::Box as Box_;
use std::pin::Pin;
use std::ptr;

pub trait ProcessManualExt {
    #[doc(alias = "media_art_process_new")]
    fn new() -> Result<Process, glib::Error>;

    #[doc(alias = "media_art_process_buffer_async")]
    fn buffer_async<
        P: IsA<gio::File>,
        Q: IsA<gio::Cancellable>,
        R: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        related_file: &P,
        buffer: &[u8],
        mime: Option<&str>,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&Q>,
        callback: R,
    );

    fn buffer_async_future<
        P: IsA<gio::File>,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        related_file: &P,
        buffer: &[u8],
        mime: Option<&str>,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>>;

    #[doc(alias = "media_art_process_file_async")]
    fn file_async<
        P: IsA<gio::File>,
        Q: IsA<gio::Cancellable>,
        R: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        file: &P,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&Q>,
        callback: R,
    );

    fn file_async_future<
        P: IsA<gio::File>,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        file: &P,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>>;

    #[doc(alias = "media_art_process_uri_async")]
    fn uri_async<
        P: IsA<gio::Cancellable>,
        Q: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        uri: &str,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&P>,
        callback: Q,
    );

    fn uri_async_future(
        &self,
        type_: Type,
        flags: ProcessFlags,
        uri: &str,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>>;
}

impl<O: IsA<Process>> ProcessManualExt for O {
    fn new() -> Result<Process, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::media_art_process_new(&mut error);
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn buffer_async<
        P: IsA<gio::File>,
        Q: IsA<gio::Cancellable>,
        R: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        related_file: &P,
        buffer: &[u8],
        mime: Option<&str>,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&Q>,
        callback: R,
    ) {
        let user_data: Box_<R> = Box_::new(callback);
        unsafe extern "C" fn buffer_async_trampoline<
            R: FnOnce(Result<(), glib::Error>) + Send + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = ptr::null_mut();
            let _ = ffi::media_art_process_buffer_finish(
                _source_object as *mut _,
                res,
                &mut error,
            );
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<R> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = buffer_async_trampoline::<R>;
        let len = buffer.len() as usize;
        unsafe {
            ffi::media_art_process_buffer_async(
                self.as_ref().to_glib_none().0,
                type_.into_glib(),
                flags.into_glib(),
                related_file.as_ref().to_glib_none().0,
                buffer.to_glib_none().0,
                len,
                mime.to_glib_none().0,
                artist.to_glib_none().0,
                title.to_glib_none().0,
                io_priority.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn buffer_async_future<
        P: IsA<gio::File>,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        related_file: &P,
        buffer: &[u8],
        mime: Option<&str>,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let related_file = related_file.clone();
        let buffer = Vec::from(buffer);
        let mime = mime.map(ToOwned::to_owned);
        let artist = artist.map(ToOwned::to_owned);
        let title = title.map(ToOwned::to_owned);
        Box_::pin(gio::GioFuture::new(
            self,
            move |obj, cancellable, send| {
                obj.buffer_async(
                    type_,
                    flags,
                    &related_file,
                    &buffer,
                    mime.as_ref().map(::std::borrow::Borrow::borrow),
                    artist.as_ref().map(::std::borrow::Borrow::borrow),
                    title.as_ref().map(::std::borrow::Borrow::borrow),
                    io_priority,
                    Some(cancellable),
                    move |res| {
                        send.resolve(res);
                    },
                );
            },
        ))
    }

    fn file_async<
        P: IsA<gio::File>,
        Q: IsA<gio::Cancellable>,
        R: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        file: &P,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&Q>,
        callback: R,
    ) {
        let user_data: Box_<R> = Box_::new(callback);
        unsafe extern "C" fn file_async_trampoline<
            R: FnOnce(Result<(), glib::Error>) + Send + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = ptr::null_mut();
            let _ = ffi::media_art_process_file_finish(
                _source_object as *mut _,
                res,
                &mut error,
            );
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<R> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = file_async_trampoline::<R>;
        unsafe {
            ffi::media_art_process_file_async(
                self.as_ref().to_glib_none().0,
                type_.into_glib(),
                flags.into_glib(),
                file.as_ref().to_glib_none().0,
                artist.to_glib_none().0,
                title.to_glib_none().0,
                io_priority.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn file_async_future<
        P: IsA<gio::File>,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        file: &P,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let file = file.clone();
        let artist = artist.map(ToOwned::to_owned);
        let title = title.map(ToOwned::to_owned);
        Box_::pin(gio::GioFuture::new(
            self,
            move |obj, cancellable, send| {
                obj.file_async(
                    type_,
                    flags,
                    &file,
                    artist.as_ref().map(::std::borrow::Borrow::borrow),
                    title.as_ref().map(::std::borrow::Borrow::borrow),
                    io_priority,
                    Some(cancellable),
                    move |res| {
                        send.resolve(res);
                    },
                );
            },
        ))
    }

    fn uri_async<
        P: IsA<gio::Cancellable>,
        Q: FnOnce(Result<(), glib::Error>) + Send + 'static,
    >(
        &self,
        type_: Type,
        flags: ProcessFlags,
        uri: &str,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
        cancellable: Option<&P>,
        callback: Q,
    ) {
        let user_data: Box_<Q> = Box_::new(callback);
        unsafe extern "C" fn uri_async_trampoline<
            Q: FnOnce(Result<(), glib::Error>) + Send + 'static,
        >(
            _source_object: *mut glib::gobject_ffi::GObject,
            res: *mut gio::ffi::GAsyncResult,
            user_data: glib::ffi::gpointer,
        ) {
            let mut error = ptr::null_mut();
            let _ = ffi::media_art_process_uri_finish(
                _source_object as *mut _,
                res,
                &mut error,
            );
            let result = if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            };
            let callback: Box_<Q> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = uri_async_trampoline::<Q>;
        unsafe {
            ffi::media_art_process_uri_async(
                self.as_ref().to_glib_none().0,
                type_.into_glib(),
                flags.into_glib(),
                uri.to_glib_none().0,
                artist.to_glib_none().0,
                title.to_glib_none().0,
                io_priority.into_glib(),
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                Some(callback),
                Box_::into_raw(user_data) as *mut _,
            );
        }
    }

    fn uri_async_future(
        &self,
        type_: Type,
        flags: ProcessFlags,
        uri: &str,
        artist: Option<&str>,
        title: Option<&str>,
        io_priority: glib::Priority,
    ) -> Pin<Box_<dyn std::future::Future<Output = Result<(), glib::Error>> + 'static>> {
        let uri = String::from(uri);
        let artist = artist.map(ToOwned::to_owned);
        let title = title.map(ToOwned::to_owned);
        Box_::pin(gio::GioFuture::new(
            self,
            move |obj, cancellable, send| {
                obj.uri_async(
                    type_,
                    flags,
                    &uri,
                    artist.as_ref().map(::std::borrow::Borrow::borrow),
                    title.as_ref().map(::std::borrow::Borrow::borrow),
                    io_priority,
                    Some(cancellable),
                    move |res| {
                        send.resolve(res);
                    },
                );
            },
        ))
    }
}
