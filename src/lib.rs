// Take a look at the license at the top of the repository in the LICENSE file.
#![cfg_attr(feature = "dox", feature(doc_cfg))]
// Re-export -sys
pub use ffi;

extern crate gio;

// No-op, because libmediaart does not have a main context
macro_rules! assert_initialized_main_thread {
    () => {};
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(clippy::clone_on_copy)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
#[allow(unused_doc_comments)]
#[allow(unused_imports)]

mod auto;

pub use glib::Error;

pub use crate::auto::*;

mod process;

pub mod prelude;
